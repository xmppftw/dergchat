# Dergchat

This is Dergchat, a small chat client for the XMPP protocol. It is written in Rust, based on `xmpp-rs` and Dioxus.

## State of the project

This project is extremely early-days. Do not use this for anything serious.

## Building

Currently, simply open the development shell defined in the flake.nix file; this should give you a shell
with all the dependencies installed. Then, run `cargo build` to build the project, and `cargo run` to run it.

Installation without Nix is not supported at this time, but you should be able to install the dependencies
through `apt` or a similar package manager.

If you experience a blank screen on linux, try setting `WEBKIT_DISABLE_COMPOSITING_MODE=1`.

## Contact

This project can be discussed on XMPP at `dergchat@conference.mizah.xyz`, or in the issues on [Codeberg](https://codeberg.org/Mizah/Dergchat).

## Contributing

I do not currently consider the project to be of sufficient maturity to invite contributions, but if you do wish to contribute, feel free to open an issue, a pull request, or talk to us on the XMPP chat mentioned above.