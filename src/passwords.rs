// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use jid::BareJid;
use keyring::Entry;
use std::fmt::Debug;

pub struct Password(pub String);

impl Debug for Password {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Password")
            .field("password", &"********")
            .finish()
    }
}

/// Retrieve the password for the given username from the keyring, if it exists.
pub fn try_retrieve_password_from_keyring(username: &BareJid) -> Option<Password> {
    Entry::new("dergchat", &username.to_string())
        .expect("Failed to create keyring entry.")
        .get_password()
        .ok()
        .map(|p| Password(p))
}

/// Store the password for the given username in the keyring.
pub fn store_keyring_password(username: &BareJid, password: &Password) {
    let entry =
        Entry::new("dergchat", &username.to_string()).expect("Failed to create keyring entry.");
    entry
        .set_password(&*password.0)
        .expect("Failed to set password in keyring.");
}
