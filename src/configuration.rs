// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::passwords::{try_retrieve_password_from_keyring, Password};
use crate::types::LoginCredentials;
use jid::BareJid;
use keyring::Entry;
use log::{error, info};
use serde_derive::{Deserialize, Serialize};

/// The configuration struct containing all the configuration options.
#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Configuration {
    pub stored_username: Option<BareJid>,
    pub stored_default_nick: Option<String>,
}

/// Cache the login attempt into the config and keyring.
///
/// Specifically, we store the username and default nick in the config file, and the password in the keyring.
///
/// Note: we do NOT store the password in the config file, because the latter is stored in plaintext.
///
/// # Arguments
/// * `attempt` - The login attempt to store.
/// * `config` - The current app configuration; UseState should prevent race conditions in writing.
pub fn store_login_details(
    jid: BareJid,
    default_nick: String,
    password: &Password,
    c: &mut Configuration,
) {
    // Open the config state and store the username and default nick.
    // Open the keyring and store the password.
    let entry = Entry::new("dergchat", &jid.to_string()).expect("Failed to create keyring entry.");
    entry
        .set_password(&password.0)
        .expect("Failed to set password in keyring.");

    // Store the username and default nick in the config.
    c.stored_username = Some(jid);
    c.stored_default_nick = Some(default_nick);

    if let Err(e) = confy::store("dergchat", None, c) {
        error!("Failed to store the config file: {}", e);
    }
}

/// Load the configuration from the config file.
///
/// On failure, log the error and return the default configuration.
pub fn load_config() -> Configuration {
    match confy::load("dergchat", None) {
        Ok(x) => x,
        Err(e) => {
            error!("Failed to load config file: {}", e);
            Configuration::default()
        }
    }
}

/// Retrieve the full login credentials from the config and keyring, if they exist.
pub fn try_retrieve_credentials(config: &Configuration) -> Option<LoginCredentials> {
    if let (Some(user), Some(nick)) = (&config.stored_username, &config.stored_default_nick) {
        if let Some(password) = try_retrieve_password_from_keyring(&user) {
            Some(LoginCredentials {
                username: user.clone(),
                default_nick: nick.clone(),
                password,
            })
        } else {
            info!("No stored password found; will not try to log in.");
            None
        }
    } else {
        info!("No stored username or default nick found; will not try to log in.");
        None
    }
}
