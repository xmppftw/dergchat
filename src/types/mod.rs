// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::passwords::Password;
use chrono::{DateTime, FixedOffset};
use jid::BareJid;
use message::{Message, MessageInOut};
use std::fmt::Debug;

pub mod message;
pub mod room;

#[derive(Debug, Clone, PartialEq)]
pub struct DirectMessageRoom {
    pub messages: Vec<DirectMessage>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct MucRoom {
    pub messages: Vec<MucMessage>,
}

pub trait Room {
    type Message: Message;

    fn messages(&self) -> &Vec<Self::Message>;
}

impl Room for DirectMessageRoom {
    type Message = DirectMessage;

    fn messages(&self) -> &Vec<Self::Message> {
        &self.messages
    }
}

impl Room for MucRoom {
    type Message = MucMessage;

    fn messages(&self) -> &Vec<Self::Message> {
        &self.messages
    }
}

/// A nickname for a user.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Nickname(pub String);

#[derive(Debug, Clone, PartialEq)]
pub struct MucMessage {
    pub in_out: MessageInOut,
    pub sender_nick: Nickname,
    pub body: String,
    pub timestamp: DateTime<FixedOffset>,
    pub is_private: bool,
}

#[derive(Debug, Clone, PartialEq)]
pub struct DirectMessage {
    pub in_out: MessageInOut,
    pub body: String,
    pub timestamp: DateTime<FixedOffset>,
}

#[derive(Debug)]
pub struct LoginCredentials {
    pub username: BareJid,
    pub default_nick: String,
    pub password: Password,
}

#[derive(Debug, Clone, PartialEq)]
pub struct MessageDeliveryError {
    pub error: String,
}
