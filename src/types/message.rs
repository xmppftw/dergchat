// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::types::{DirectMessage, MessageDeliveryError, MucMessage, Nickname};
use chrono::{DateTime, FixedOffset};

/// An outgoing status for a message.
///
/// See: https://docs.modernxmpp.org/client/design/#conversation-view
#[derive(Debug, Clone, PartialEq)]
pub enum OutgoingMessageStatus {
    PendingDelivery,
    DeliveredToServer,
    DeliveredToContact,
    ReadByContact,
    DeliveryError(MessageDeliveryError),
}

/// An enum type signaling whether a message is incoming or outgoing.
#[derive(Debug, Clone, PartialEq)]
pub enum MessageInOut {
    /// An incoming message.
    Incoming,
    /// An outgoing message, with a status.
    Outgoing(OutgoingMessageStatus),
}

/// A message, either incoming or outgoing.
///
/// This is a trait, because MUC messages and direct messages are subtly different.
pub trait Message {
    /// Whether the message is incoming or outgoing.
    /// If outgoing, also contains the status of the message.
    fn in_out(&self) -> &MessageInOut;

    /// The body of the message, as a string. (TODO: Language?)
    fn body(&self) -> &String;

    /// The timestamp of the message; when it was sent originally.
    fn timestamp(&self) -> &DateTime<FixedOffset>;

    fn sender_nick(&self) -> Nickname;
}

impl Message for MucMessage {
    fn in_out(&self) -> &MessageInOut {
        &self.in_out
    }

    fn body(&self) -> &String {
        &self.body
    }

    fn timestamp(&self) -> &DateTime<FixedOffset> {
        &self.timestamp
    }

    fn sender_nick(&self) -> Nickname {
        self.sender_nick.clone()
    }
}

impl Message for DirectMessage {
    fn in_out(&self) -> &MessageInOut {
        &self.in_out
    }

    fn body(&self) -> &String {
        &self.body
    }

    fn timestamp(&self) -> &DateTime<FixedOffset> {
        &self.timestamp
    }

    fn sender_nick(&self) -> Nickname {
        Nickname("Not implemented.".to_string())
    }
}
