// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::types::{DirectMessageRoom, MucRoom};

#[derive(Debug, Clone, PartialEq)]
pub enum Room {
    Direct(DirectMessageRoom),
    Muc(MucRoom),
}

#[derive(Debug, Clone, PartialEq)]
pub struct WrongRoomType;

impl<'a> TryInto<&'a mut DirectMessageRoom> for &'a mut Room {
    type Error = WrongRoomType;

    fn try_into(self) -> Result<&'a mut DirectMessageRoom, Self::Error> {
        match self {
            Room::Direct(room) => Ok(room),
            _ => Err(WrongRoomType),
        }
    }
}

impl<'a> TryInto<&'a mut MucRoom> for &'a mut Room {
    type Error = WrongRoomType;

    fn try_into(self) -> Result<&'a mut MucRoom, Self::Error> {
        match self {
            Room::Muc(room) => Ok(room),
            _ => Err(WrongRoomType),
        }
    }
}
