// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![allow(non_snake_case)]

mod configuration;
mod passwords;
mod types;
mod widgets;
mod xmpp_interface;

use dioxus::prelude::*;
use dioxus_desktop::Config;

use widgets::App;

const STYLESHEET: &str = r#"
    body {
        margin: 0;
        padding: 0;
        position: fixed;
        width: 100%;
        height: 100%;
        color: lightgray;
    }

    #main {
        display: flex;
        flex-direction: row;
        width: 100%;
        height: 100%;
    }

    .login-form {
        margin: auto;
        text-align: center;
    }

    .login-form input[type=text], .login-form input[type=password] {
        display: block;
        padding: 2mm;
        margin: 2mm 0;
    }

    .login-form input[type=checkbox] {
        vertical-align: middle;
        padding: 2mm;
        margin: 2mm;
    }

    .login-form button {
        display: block;
        padding: 2mm;
        margin: 2mm 0;
        width: 100%;
        box-sizing: border-box;
    }
"#;

fn main() {
    env_logger::init();

    // launch the dioxus app in a webview
    dioxus_desktop::launch_cfg(
        App,
        Config::default()
            .with_custom_head(format!(r#"<style>{}</style>"#, STYLESHEET))
            .with_background_color((32, 64, 32, 255)),
    );
}
