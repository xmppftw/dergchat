// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::widgets::room_join_widget::RoomJoinWidget;
use crate::widgets::room_list::{RoomList, RoomMeta};
use dioxus::core::{Element, Scope};
use dioxus::core_macro::Props;
use dioxus::html::text;
use dioxus::prelude::*;
use jid::BareJid;

/// A widget that combines the RoomList, RoomJoinWidget, and current user info into a sidebar.
#[component]
pub fn SideBar<'a>(
    cx: Scope<'a, SideBarProps>,
    current_user: BareJid,
    rooms: Vec<RoomMeta>,
    on_room_picked: EventHandler<'a, BareJid>,
    on_room_left: EventHandler<'a, BareJid>,
    on_join_room: EventHandler<'a, BareJid>,
    on_logout: EventHandler<'a, ()>,
) -> Element<'a> {
    render! {
        div {
            padding: "5mm",
            background_color: "#1d852d",
            display: "flex",
            flex_direction: "column",

            // The name of the current user (TODO: make this actually reflect the current user).
            div {
                border_bottom: "1px solid lightgray",

                "{current_user}",

                button {
                    onclick: move |_| on_logout.call(()),
                    "Log out",
                }
            }

            // The list of rooms.
            RoomList {
                rooms: cx.props.rooms.clone(),
                on_room_picked: |e| cx.props.on_room_picked.call(e),
                on_room_left: |e| cx.props.on_room_left.call(e),
            }

            // The widget to join a room.
            RoomJoinWidget {
                on_join_room: |x| cx.props.on_join_room.call(x),
            }
        }
    }
}
