// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use dioxus::core::{Element, Scope};
use dioxus::hooks::use_state;
use dioxus::prelude::*;

use crate::passwords::Password;
use crate::types::LoginCredentials;
use jid::BareJid;
use log::error;
use std::fmt::Debug;
use std::str::FromStr;

#[derive(Debug, Clone, PartialEq)]
pub enum LoginStatus {
    LoggedOut,
    LoggingIn,
    LoggedIn(BareJid),
    Error(String),
}

/// A struct that represents a login attempt, containing a username, a default nick, and a password.
/// None are validated in any way.
///
/// Warning: this struct contains the user's password; keep it in memory only as short as possible.
#[derive(Clone)]
pub struct LoginAttempt {
    pub username: String,
    pub default_nick: String,
    pub password: String,
}

impl Debug for LoginAttempt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("LoginAttempt")
            .field("username", &self.username)
            .field("default_nick", &self.default_nick)
            .field("password", &"********")
            .finish()
    }
}

pub fn validate_login_attempt(x: LoginAttempt) -> Result<LoginCredentials, String> {
    let LoginAttempt {
        username,
        default_nick,
        password,
    } = x;

    // First, validate the username as a jid:
    let jid = match BareJid::from_str(&username) {
        Ok(x) => x,
        Err(e) => {
            error!("Invalid JID: {}", e);
            return Err(format!("Invalid JID: {}", e));
        }
    };

    // Wrap the password in a Password struct.
    let password = Password(password);

    Ok(LoginCredentials {
        username: jid,
        default_nick,
        password,
    })
}

/// The props for the login screen widget, including:
/// * The xmpp address to pre-fill in the username field.
/// * The default nick to pre-fill in the default nick field.
/// * The current login state, which will change as the user attempts to log in.
/// * The event handler for when the user clicks the login button.
#[derive(Props)]
pub struct LoginScreenProps<'a> {
    cached_username: String,
    cached_nick: String,
    login_state: LoginStatus,
    on_login_attempt: EventHandler<'a, LoginAttempt>,
}

/// A login screen widget.
///
/// This widget is used to query the user for their login credentials, namely:
/// * Username (XMPP address/Jid)
/// * Default nick
/// * Password
///
/// The default nick and username may be pre-filled from the cache.
///
/// The widget itself only contains the UI, and does not know how to validate the credentials
/// or log the user in; the parent context is responsible for that.
///
/// As a small UX improvement, the widget will automatically fill in the default nick
/// based on the username, if the user has not changed the default nick.
pub fn LoginScreen<'a>(cx: Scope<'a, LoginScreenProps>) -> Element<'a> {
    // The username.
    let username = use_state(cx, || cx.props.cached_username.clone());

    // The default nick.
    let default_nick = use_state(cx, || cx.props.cached_nick.clone());

    // The password.
    let password = use_state(cx, || "".to_string());

    // Track whether the default nick was directly changed by the user.
    let default_nick_was_changed = use_state(cx, || false);

    render! {

        div {
            class: "login-form",

            // The username input.
            input {
                "type": "text",
                placeholder: "XMPP Address",
                value: "{username}",
                oninput: move |x| {

                    // Set the username state variable.
                    username.set(x.value.clone());

                    // If the default nick was not changed by the user, set it to the part before the @.
                    if !default_nick_was_changed.get() {
                        if let Some(before_at) = x.value.split('@').next() {
                            default_nick.set(before_at.to_string());
                        }
                    }
                },
            }

            // The default nick input.
            input {
                placeholder: "Default nick",
                "type": "text",
                value: "{default_nick}",
                oninput: move |x| {
                    // Store the nick in the state variable.
                    default_nick.set(x.value.clone());

                    // Mark the nick as changed, so that it won't be overwritten by the username.
                    default_nick_was_changed.set(true);
                },
            }

            // The password input.
            input {
                placeholder: "Password",
                "type": "password",
                oninput: move |x| {
                    password.set(x.value.clone());
                },
            }

            if let LoginStatus::Error(e) = &cx.props.login_state {
                rsx! {
                    div {
                        class: "error",
                        "{e}"
                    }
                }
            }

            rsx! {
                button {

                    disabled: match cx.props.login_state {
                        LoginStatus::LoggedIn(_) => true,
                        LoginStatus::LoggedOut => false,
                        LoginStatus::LoggingIn => true,
                        LoginStatus::Error(_) => false,
                    },

                    onclick: move |_| {
                        cx.props.on_login_attempt.call(LoginAttempt {
                            username: username.current().to_string(),
                            default_nick: default_nick.current().to_string(),
                            password: password.current().to_string(),
                        });
                    },

                    match cx.props.login_state {
                        LoginStatus::LoggedOut => "Log in",
                        LoginStatus::LoggingIn => "Logging in...",
                        LoginStatus::LoggedIn(_) => "Logged in",
                        LoginStatus::Error(_) => "Log in",
                    }
                }

            }
        }
    }
}
