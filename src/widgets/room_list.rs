// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use chrono::{DateTime, FixedOffset};
use dioxus::core::{Element, Scope};
use dioxus::core_macro::Props;
use dioxus::html::button;
use dioxus::prelude::*;
use jid::BareJid;
use std::time::Instant;

#[derive(Clone, Eq, PartialEq)]
pub struct RoomMeta {
    pub room: BareJid,
    pub last_update_time: Option<DateTime<FixedOffset>>,
}

#[derive(Clone, Eq, PartialEq, Copy)]
pub enum SortMethod {
    Alphabetical,
    Recency,
}

/// A widget that shows a list of rooms known to the client.
///
/// It provides an overview of the rooms, and allows the user to
/// select a room or to leave it.
///
/// How "selecting" and "leaving" a room is handled is up to the context.
#[component]
pub fn RoomList<'a>(
    cx: Scope<'a>,
    rooms: Vec<RoomMeta>, // TODO: Should this be a reference of some kind?
    on_room_picked: EventHandler<'a, BareJid>,
    on_room_left: EventHandler<'a, BareJid>,
) -> Element<'a> {
    let sort_method = use_state(cx, || SortMethod::Recency);

    let sorted_rooms = use_memo(cx, (rooms, sort_method), |(mut rooms, sort_method)| {
        match *sort_method.current() {
            SortMethod::Alphabetical => rooms.sort_by_key(|room| room.room.to_string()),
            SortMethod::Recency => rooms.sort_by_key(|room| room.last_update_time),
        }

        rooms
    });

    render! {

        button {
            onclick: move |_| {
                match *sort_method.current() {
                    SortMethod::Alphabetical => sort_method.set(SortMethod::Recency),
                    SortMethod::Recency => sort_method.set(SortMethod::Alphabetical),
                }
            },
            "Sort by: ",
            match *sort_method.current() {
                SortMethod::Alphabetical => "alphabetical",
                SortMethod::Recency => "recency",
            }
        }

        // An <ul> with a list of <li> elements, each containing a room name and a button.
        ul {
            list_style: "none",
            flex_grow: 1,
            margin: 0,
            padding: 0,
            for room in cx.props.rooms.iter() {
                // A single <li> element, containing a room name and a button.

                rsx! { li {
                    display: "flex",
                    flex_direction: "row",

                    onclick: |_evt| cx.props.on_room_picked.call(room.room.clone()),

                    // The room name inside a <div>.
                    div {
                        flex_grow: 1,
                        "{room.room}"
                    }

                    // A button to leave the room.
                    button {

                        onclick: |evt| {
                            evt.stop_propagation();
                            cx.props.on_room_left.call(room.room.clone());
                        },
                        "X"
                    }
                } }
            }
        }
    }
}
