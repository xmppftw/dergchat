// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use dioxus::core::{Element, Scope};
use dioxus::core_macro::Props;
use dioxus::hooks::use_state;
use dioxus::prelude::*;

#[derive(Props)]
pub struct SendMessageProps<'a> {
    on_message_sent: EventHandler<'a, String>,
}

/// A simple widget that allows the user to compose a message and a button to send it.
pub fn SendMessage<'a>(cx: Scope<'a, SendMessageProps>) -> Element<'a> {
    // The message body being written.
    let message = use_state(cx, || "".to_owned());

    render! {
        div {
            class: "send-message-widget",
            display: "flex",
            flex_direction: "row",

            // A non-resizable text area for the message body.
            textarea {
                resize: "none",
                flex_grow: 1,
                oninput: |evt| {
                    message.set(evt.value.clone());
                }
            }

            // A button to send the message.
            button {
                height: "100%",
                onclick: move |_| {
                    cx.props.on_message_sent.call(message.current().to_string());
                },
                "Send"
            }
        }
    }
}
