// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use dioxus::core::{Element, Scope};
use dioxus::core_macro::Props;
use dioxus::hooks::use_state;
use dioxus::prelude::*;
use jid::BareJid;
use std::str::FromStr;

/// The props for the room join widget, including:
/// * The event handler for when the user clicks the join button, passing in a room name. This name is not validated in any way.
#[derive(Props)]
pub struct RoomJoinProps<'a> {
    on_join_room: EventHandler<'a, BareJid>,
}

/// A simple widget that allows the user to join a room by typing
/// its name into a text field and clicking a button.
///
/// Also validates the room name, and displays an error message if the room name is invalid.
pub fn RoomJoinWidget<'a>(cx: Scope<'a, RoomJoinProps>) -> Element<'a> {
    // Store the current room name and error message in state.
    let room = use_state(cx, || "".to_owned());
    let error = use_state(cx, || "".to_owned());

    render! {
        div {
            display: "flex",
            flex_direction: "row",
            input {
                flex_grow: 1,
                display: "block",
                oninput: |evt| {
                    room.set(evt.value.to_string());
                    error.set("".to_owned());
                }
            }
            button {
                onclick: move |_| {
                    // Validate the room name. If it's a valid Jid, try to join it.
                    // Otherwise, display an error message.
                    match BareJid::from_str(room.current().as_str()) {
                        Ok(jid) => {error.set("".to_string()); cx.props.on_join_room.call(jid);},
                        Err(e) => {error.set(e.to_string());},
                    }
                },
                "Join"
            }
        }
    }
}
