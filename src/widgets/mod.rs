// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::types::room::Room;
use crate::xmpp_interface::Messages;
use crate::{
    configuration::store_login_details,
    configuration::{load_config, try_retrieve_credentials},
    widgets::{
        login_screen::validate_login_attempt,
        login_screen::LoginAttempt,
        login_screen::{LoginScreen, LoginStatus},
        no_room_open::NoRoomPlaceholder,
        room_list::RoomMeta,
        room_view::RoomView,
        sidebar::SideBar,
    },
    xmpp_interface,
    xmpp_interface::NetworkCommand,
};
use dioxus::core::{Element, Scope};
use dioxus::prelude::*;
use futures_util::StreamExt;
use jid::BareJid;
use log::{error, info};
use std::string::String;

pub mod login_screen;
pub mod no_room_open;
pub mod room_join_widget;
pub mod room_list;
pub mod room_view;
pub mod send_message;
pub mod sidebar;

pub fn App(cx: Scope) -> Element {
    let messages = use_ref(cx, || Messages::new());
    let current_room = use_state(cx, || None::<BareJid>);
    let connection_status = use_state(cx, || LoginStatus::LoggedOut);

    let coroutine = use_coroutine(cx, |rx: UnboundedReceiver<NetworkCommand>| {
        xmpp_interface::run_xmpp_toplevel(connection_status.to_owned(), messages.to_owned(), rx)
    });

    let config = use_ref(cx, load_config);

    use_on_create(cx, || {
        let config = config.to_owned();
        let _connection_status = connection_status.to_owned();
        let coroutine = coroutine.to_owned();
        async move {
            if let Some(credentials) = try_retrieve_credentials(&*config.read()) {
                info!(
                    "Will try to log in as {} with default nick {}",
                    credentials.username, credentials.default_nick
                );
                coroutine.send(NetworkCommand::TryLogin { credentials });
            } else {
                info!("No stored credentials found; will not try to automatically log in.");
            }
        }
    });

    // Can I not clone this? Am I reconstructing the entire VDOM every time? What's happening?
    // TODO: The borrowing situation here is kinda fucked; how do I fix this?
    let current_room_messages = current_room
        .get()
        .clone()
        .and_then(|x| messages.read().messages.get(&x).map(|x| x.clone()));

    render! {

        // If not logged in, show a login screen.
        if let LoginStatus::LoggedIn(user) = connection_status.get() {

            let room_list = messages.read().messages.iter().map(|(k, v)| {
                        match v {
                            Room::Direct(d) => RoomMeta {
                                room: k.clone(),
                                last_update_time: d.messages.last().map(|x| x.timestamp).map(|x| x.clone()),
                            },
                            Room::Muc(m) => RoomMeta {
                                room: k.clone(),
                                last_update_time: m.messages.last().map(|x| x.timestamp).map(|x| x.clone()),
                            },
                        }
                    }).collect();

            // We're logged in; show a sidebar and a room view.
            rsx!{

                // Sidebar.
                SideBar {
                    current_user: user.clone(),
                    rooms: room_list,
                    on_room_picked: move |x: BareJid| {
                        current_room.set(Some(x.clone()));
                        coroutine.send(NetworkCommand::JoinRoom { room: x });
                    },
                    on_room_left: move |x: BareJid| {
                        current_room.set(None);
                        coroutine.send(NetworkCommand::LeaveRoom { room: x });
                    },
                    on_join_room: move |x: BareJid| {
                        coroutine.send(NetworkCommand::JoinRoom { room: x });
                    },
                    on_logout: move |_| {
                        connection_status.set(LoginStatus::LoggedOut);
                        current_room.set(None);
                        coroutine.send(NetworkCommand::Logout);
                    },
                }

                // The current room.
                if let Some(room) = current_room_messages {
                    match (room) {
                    Room::Direct(r) => {
                        // Direct message room
                        rsx! {
                            RoomView {
                                room: current_room.get().clone().unwrap(),
                                messages: r,
                                on_message_sent: move |x:String| {
                                    println!("Message sent: {:?}", x);
                                    coroutine.send(NetworkCommand::SendMessage { recipient: current_room.get().clone().unwrap(), message: x });
                                },
                            }
                        }
                    },
                    Room::Muc(r) => {
                        // MUC room
                        rsx! {
                            RoomView {
                                room: current_room.get().clone().unwrap(),
                                messages: r,
                                on_message_sent: move |x:String| {
                                    println!("Message sent: {:?}", x);
                                    coroutine.send(NetworkCommand::SendMessage { recipient: current_room.get().clone().unwrap(), message: x });
                                },
                            }
                        }
                    },
                    }
                } else {
                    // No room selected
                    rsx! {
                        NoRoomPlaceholder {}
                    }
                }
            }
        } else {
            rsx! {
                LoginScreen {
                    cached_username: config.read().stored_username.clone().map(|x| x.to_string()).unwrap_or("".to_string()),
                    cached_nick: config.read().stored_default_nick.clone().unwrap_or("".to_string()),
                    login_state: connection_status.current().as_ref().clone(),
                    on_login_attempt: move |x: LoginAttempt| {
                        // Validate the login attempt.
                        match validate_login_attempt(x) {
                            Ok(x) => {
                                config.with_mut(|c| {
                                    store_login_details(x.username.clone(), x.default_nick.clone(), &x.password, c);
                                });
                                coroutine.send(NetworkCommand::TryLogin { credentials: x });
                            },
                            Err(e) => {
                                error!("Invalid login attempt: {}", e);
                            }
                        }
                    },
                }
            }
        }
    }
}
