// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::types::message::Message;
use crate::types::Room;
use crate::widgets::send_message::SendMessage;
use dioxus::core::{Element, Scope};
use dioxus::core_macro::Props;
use dioxus::prelude::*;
use jid::BareJid;

/// A widget that shows a room, including a list of messages and a widget to send new messages.
#[component]
pub fn RoomView<'a, R: Room>(
    cx: Scope<'a>,
    room: BareJid,
    messages: R,
    on_message_sent: EventHandler<'a, String>,
) -> Element<'a> {
    render! {
        div {
            padding: "5mm",
            flex_grow: 1,
            display: "flex",
            background_color: "#166322",
            flex_direction: "column",

            // The room name, as an H2 (H1 is reserved for the app name).
            h2 {
                margin: 0,
                padding: 0,
                border_bottom: "1px solid lightgray",
                "{cx.props.room}"
            }

            // The list of messages, as an <ul>, where each message is an <li>.
            ul {
                flex_grow: 1,
                overflow_y: "scroll",
                for message in cx.props.messages.messages().iter() {
                    rsx! { li {
                        "{message.sender_nick().0}: {message.body()}"
                    } }
                }
            }

            // The widget to compose new messages and send them.
            SendMessage {
                on_message_sent: |x:String| cx.props.on_message_sent.call(x),
            }
        }
    }
}
