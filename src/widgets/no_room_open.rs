// Dergchat, a free XMPP client.
// Copyright (C) 2023 Werner Kroneman
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use dioxus::core::{Element, Scope};
use dioxus::prelude::*;

/// A placeholder widget for when no room is open.
pub fn NoRoomPlaceholder(cx: Scope) -> Element {
    render! {
        div {
            padding: "5mm",
            flex_grow: 1,
            display: "flex",
            background_color: "#166322",
            "No room selected. Pick one from the list on the left."
        }
    }
}
