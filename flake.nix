# Dergchat, a free XMPP client.
# Copyright (C) 2023 Werner Kroneman
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

{
  description = "A devshell environment for an XMPP client thingy.";

  nixConfig.bash-prompt = "[rust/chatthingy] ";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    rust-toolchain = pkgs.symlinkJoin {
      name = "rust-toolchain";
      paths = [pkgs.rustc pkgs.cargo pkgs.rustPlatform.rustcSrc];
    };
  in
  {
    devShells.${system}.default =
      pkgs.mkShell rec {

        packages = with pkgs; [
          rust-toolchain
          pkg-config
          rustfmt
          openssl
          glibc
          gtk3
          webkitgtk_4_1
        ];

        WEBKIT_DISABLE_COMPOSITING_MODE=1;

         RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
      };
    packages.${system}.default = pkgs.rustPlatform.buildRustPackage {
      name = "Dergchat";
      src = pkgs.lib.cleanSource ./.;
      nativeBuildInputs = with pkgs; [
                      pkg-config
                    ];
    buildInputs = with pkgs; [
                    rust-toolchain
                    pkg-config
                    openssl
                    glibc
                    gtk3
                    webkitgtk_4_1
                  ];
      cargoLock = {
        lockFile = ./Cargo.lock;
        outputHashes = {
            "xmpp-0.5.0" = "sha256-zRHI2qSCLgI/mrRrToRrDbvZYSa9vwxoDwghzNyN4mg=";
        };
      };
    };
  };
}
